Attribute VB_Name = "FCSFunctions"
Option Explicit 'force to declare all variables

Public Grid As AGrid 'the class that contains stage and FCS positions
Public FcsControl As AimFcsController
Public ViewerGuiServer As AimViewerGuiServer
Public FcsPositions As AimFcsSamplePositionParameters
Public AcquisitionController  As AimScanController

''
' Starts the form
''
Public Sub FcsRunner_Setup()
    ZenV = getVersionNr
    'find the version of the software
#If ZENvC >= 2012 Then
    If ZenV < 2012 Then
        MsgBox "ZENvC Compiler constant is set to 2012 but your ZEN version is below ZEN2012." & vbCrLf & _
        "Edit project properties in the VBA editor by right clicking on project name and modify conditional compiler arguments ZENvC to your ZEN version"
        Exit Sub
    End If
#Else
    If ZenV >= 2012 Then
        MsgBox "ZENvC Compiler constant is not set for 2012 or higher but your ZEN version is 2012 or higher." & vbCrLf & _
        "Edit project properties in the VBA editor by right clicking on project name and modify conditional compiler arguments ZENvC to your ZEN version"
        Exit Sub
    End If
#End If
    FCSRunner.Show
End Sub


''
' initialize the FCS classes
''
Public Function Initialize_Controller() As Boolean
    Set FcsControl = Fcs 'member of Lsm5VBAProject
    Set ViewerGuiServer = Lsm5.ViewerGuiServer
    Set FcsPositions = FcsControl.SamplePositionParameters
    'ViewerGuiServer.FcsSelectLsmImagePositions = True
End Function

'''''
'   getFcsPosition(Optional idx As Long = -1)
'   read FCS position and return it as vector
'   if idx = -1 position is obtained from GUI directly
'''''
Public Function getFcsPosition(Optional idx As Long = -1) As Vector
    If idx = -1 Then
        'read actual position of crosshair
        Set ViewerGuiServer = Lsm5.ViewerGuiServer
        ViewerGuiServer.FcsGetLsmCoordinates getFcsPosition.X, getFcsPosition.Y, getFcsPosition.Z
    Else
        If idx >= FcsPositions.PositionListSize Then
           Exit Function
        End If
        getFcsPosition.X = FcsPositions.PositionX(idx)
        getFcsPosition.Y = FcsPositions.PositionY(idx)
        getFcsPosition.Z = FcsPositions.PositionZ(idx)
    End If
End Function


''
'  getFcsPositionAbsVec(currentPosition As Vector) As Vector()
'  convert Fcs position to absolute position in um and return an array of vectors
''
Public Function getFcsPositionAbsVec(currentPosition As Vector) As Vector()
    Dim idx As Long
    Dim pos() As Vector
    If FcsPositions.PositionListSize = 0 Then
        Exit Function
    End If
    ReDim pos(0 To FcsPositions.PositionListSize - 1)
    For idx = 0 To FcsPositions.PositionListSize - 1
        pos(idx) = getFcsPosition(idx)
        pos(idx).X = Round(pos(idx).X * 1000000 + currentPosition.X, PrecXY)
        pos(idx).Y = Round(pos(idx).Y * 1000000 + currentPosition.Y, PrecXY)
        pos(idx).Z = Round(pos(idx).Z * 1000000, PrecZ)
    Next idx
    
    getFcsPositionAbsVec = pos
End Function

''
'   getFcsPositionVec() As Vector ()
'   reads FCS positions and return array of vectors
''
Public Function getFcsPositionVec() As Vector()
    Dim idx As Long
    Dim pos() As Vector
    If FcsPositions.PositionListSize = 0 Then
        Exit Function
    End If
    ReDim pos(0 To FcsPositions.PositionListSize - 1)
    For idx = 0 To FcsPositions.PositionListSize - 1
        pos(idx) = getFcsPosition(idx)
    Next idx
    getFcsPositionVec = pos
End Function

''
'   fcsPos2imgPx(Recording As DsRecording, currentPosition As Vector, fcsPos() As Vector) As Vector()
'   convert FCS positions (fcsPos) to pixel position where (X,Y,Z) = (0,0,0) is upper left corner and bottom slice.
'       Recording: current DsRecording image where FCS points have been placed
'       currentPosition: current stage (XY) and focus (Z) position in um
'       fcsPos(): array of fcs positions. FCS positions are in meter and X=0, Y=0 is center of image.
'                 Z is absolute position of focus.
''
Public Function fcsPos2imgPx(Recording As DsRecording, currentPosition As Vector, fcspos() As Vector) As Vector()
    Dim imgSizePx As Vector
    Dim imgPx() As Vector
    Dim pixelSizeXY As Double
    Dim frameSpacing As Double
    Dim i As Long
    imgPx = fcspos
    imgSizePx = imageSizePx(Recording)
    pixelSizeXY = Recording.SampleSpacing 'it is in meter for RecordingDoc.Recording but in um for Lsm5.DsRecording!
    If pixelSizeXY < 0.01 Then 'smaller than 40x smallest pixelSize in um
        pixelSizeXY = pixelSizeXY * 1000000
    End If
    frameSpacing = Recording.frameSpacing 'this is in um
    For i = 0 To UBound(fcspos)
        imgPx(i).X = fcspos(i).X / (pixelSizeXY * 0.000001) + (imgSizePx.X - 1) / 2
        imgPx(i).Y = fcspos(i).Y / (pixelSizeXY * 0.000001) + (imgSizePx.Y - 1) / 2
        If imgSizePx.Z < 2 Then
            imgPx(i).Z = 0
        Else
            imgPx(i).Z = Round((fcspos(i).Z * 1000000 - currentPosition.Z) / frameSpacing + (imgSizePx.Z - 1) / 2, 2)
        End If
    Next i
    fcsPos2imgPx = imgPx
End Function

'---------------------------------------------------------------------------------------
' Procedure : imgPx2fcsPos
' Purpose   : Compute coordinates for fcs from pixel coordinates
' Variables : Recording - current image settings
'             currentPosition - stage/focus position in um
'             imgPx()- Vector of positions in pixel (0,0,0) is upper left bottom slice
' Returns   : stage positions in meter!!! (different from computeCoordinatesImaging which returns in um)
'---------------------------------------------------------------------------------------

Public Function imgPx2fcsPos(Recording As DsRecording, currentPosition As Vector, imgPx() As Vector) As Vector()

    Dim imgSizePx As Vector
    Dim posPx() As Vector
    Dim pixelSizeXY As Double
    Dim frameSpacing As Double
    Dim i As Long
    Dim fcspos() As Vector
On Error GoTo imgPx2fcsPos_Error

    fcspos = imgPx
    imgSizePx = imageSizePx(Recording)
    pixelSizeXY = Recording.SampleSpacing 'it is in meter for RecordingDoc.Recording but in um for Lsm5.DsRecording!
    'try to guess and transform in um if recquired
    If pixelSizeXY < 0.01 Then 'smaller than 40x smallest pixelSize in um
        pixelSizeXY = pixelSizeXY * 1000000
    End If
    frameSpacing = Recording.frameSpacing 'this is in um

     
    For i = 0 To UBound(imgPx)
        'for FCS position is with respect center of image in meter
        fcspos(i).X = (fcspos(i).X - (imgSizePx.X - 1) / 2) * pixelSizeXY * 0.000001
        fcspos(i).Y = (fcspos(i).Y - (imgSizePx.Y - 1) / 2) * pixelSizeXY * 0.000001
        'add or remove half a pixel in case
        If imgSizePx.Z >= 2 Then
            fcspos(i).Z = 0
        Else
            fcspos(i).Z = (fcspos(i).Z - (imgSizePx.Z - 1) / 2) * frameSpacing
        End If
        'absolute position in meter
        fcspos(i).Z = (currentPosition.Z + fcspos(i).Z) * 0.000001
    Next i
    imgPx2fcsPos = fcspos

   On Error GoTo 0
   Exit Function

   On Error GoTo 0
   Exit Function

imgPx2fcsPos_Error:

    LogManager.UpdateErrorLog "Error " & Err.Number & " (" & Err.Description & _
    ") in procedure imgPx2fcsPos of Module FCSFunctions at line " & Erl & " "
End Function



''
' returns size of image in pixels base 0
''
Public Function imageSizePx(Recording As DsRecording) As Vector

    Dim TX As Integer
    Dim TY As Integer
   On Error GoTo imageSizePx_Error

    TX = 1
    TY = 1
    If Recording.TileAcquisition Then
        TX = Recording.TilesX
        TY = Recording.TilesY
    End If
    imageSizePx.X = Recording.SamplesPerLine * TX
    
    If Recording.ScanMode = "ZScan" Then
        imageSizePx.Y = 1
    Else
        imageSizePx.Y = Recording.LinesPerFrame * TY
    End If
    
    If isZStack(Recording) Then
        imageSizePx.Z = Recording.framesPerStack
    Else
        imageSizePx.Z = 1
    End If

   On Error GoTo 0
   Exit Function

imageSizePx_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure imageSizePx of Module FCSFunctions"
End Function

''
' returns true if acquisition is a ZStack
''
Public Function isZStack(Recording As DsRecording) As Boolean
    Dim ScanMode As String
    ScanMode = Recording.ScanMode
    If ScanMode = "ZScan" Or ScanMode = "Stack" Then
        isZStack = True
    Else
        isZStack = False
    End If
End Function



'''''
'   setFcsPosition(pos As Vector, idx As Long)
'   Update or create a new FCS Position.
'       fcsPos -  FCS positions are in meter and X=0, Y=0 is center of image.
'                 Z is absolute position of focus.
'       idx - index of Fcs position in list. If idx > FcsPositions.PositionListSize - 1 then all positions in between are set to 0,0,0
'''''
Public Function setFcsPosition(fcspos As Vector, idx As Long, Optional Recording As DsRecording = Nothing) As Boolean
    'Create only a new position if at end of list
    If idx > FcsPositions.PositionListSize Then
        Exit Function
    End If
    If Recording Is Nothing Then
        FcsPositions.PositionX(idx) = fcspos.X
        FcsPositions.PositionY(idx) = fcspos.Y
    Else
        
    End If
    FcsPositions.PositionZ(idx) = fcspos.Z
    'this shows the small crosshair
    ViewerGuiServer.UpdateFcsPositions
    setFcsPosition = True
End Function

'''''
'   setFcsPositionVec(fcsPos() As Vector)
'   set FCS positions according to vector fcsPos
'   fcsPos -  Array of fcs positons. FCS positions are in meter and X=0, Y=0 is center of image Z is absolute position of focus.
'''''
Public Sub setFcsPositionVec(fcspos() As Vector)
    Dim idx As Integer
    
    'set positions to zero if vector is empty
    If isPosArrayEmpty(fcspos) Then
        FcsPositions.PositionListSize = 0
        ViewerGuiServer.UpdateFcsPositions
        Exit Sub
    End If

    FcsPositions.PositionListSize = UBound(fcspos) + 1
    For idx = 0 To UBound(fcspos)
        FcsPositions.PositionX(idx) = fcspos(idx).X
        FcsPositions.PositionY(idx) = fcspos(idx).Y
        FcsPositions.PositionZ(idx) = fcspos(idx).Z
    Next idx
    'this shows the small crosshair
    ViewerGuiServer.UpdateFcsPositions
End Sub


''''
'   GetFcsListPositionLength()
'   Maximal number of positions
''''
Public Function GetFcsPositionListLength() As Long
    GetFcsPositionListLength = FcsPositions.PositionListSize
End Function

''''
'   ClearFcsPositionList()
'   Remove all FCSpositions stored
''''
Public Function ClearFcsPositionList()
    'This clear the positions
    FcsPositions.PositionListSize = 0
    ViewerGuiServer.UpdateFcsPositions
End Function
