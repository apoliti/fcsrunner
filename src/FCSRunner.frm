VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} FCSRunner 
   Caption         =   "FCSRunner"
   ClientHeight    =   8205
   ClientLeft      =   48
   ClientTop       =   372
   ClientWidth     =   7644
   OleObjectBlob   =   "FCSRunner.frx":0000
   ShowModal       =   0   'False
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "FCSRunner"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit 'force to declare all variables
Private Const Version = "v0.8.1"
Private IsAcquiring As Boolean
Private Counter As Integer


''''''
' UserForm_Initialize()
'   Function called from show command
'   Load and initialize form
''
Private Sub UserForm_Initialize()
    Dim i As Integer
    Dim strIconPath As String
    Dim lngIcon As Long
    Dim lnghWnd As Long
    'Setting of some global variables
    Me.Caption = Me.Caption + " " + Version
    FormatUserForm (Me.Caption) ' make minimizing button available

    ZenV = getVersionNr
    If ZenV > 2010 Then
        On Error GoTo errorMsg
        'in some cases this does not register properly
        'Set ZEN = Lsm5.CreateObject("Zeiss.Micro.AIM.ApplicationInterface.ApplicationInterface")
        'this should always work
        Set ZEN = Application.ApplicationInterface
        'Check if it works and set FCS to select new positions
        ZEN.gui.Fcs.EnablePositions.value = True
        ZEN.gui.Fcs.Positions.EnablePositionList.value = True
        ZEN.gui.Fcs.Positions.PositionListSelectPosition.value = True

        Set Focus = Lsm5.ExternalCpObject.pHardwareObjects.pFocus.pItem(0)
        If (Focus.bIsStabilizerAvailable) Then
            checkBoxDF.Visible = True
        Else
            checkBoxDF.Visible = False
        End If
            
            
        'ZEN.gui.Fcs.Positions.AddPosition.Execute
        'ZEN.gui.Fcs.Positions.PositionListRemoveAll.Execute
        GoTo NoError
errorMsg:
        Set ZEN = Nothing
        MsgBox "Version is ZEN" & ZenV & " but can't find Zeiss.Micro.AIM.ApplicationInterface." & vbCrLf & "Using ZEN2010 settings instead." & vbCrLf & "Check if Zeiss.Micro.AIM.ApplicationInterface.dll is registered?" & "See also the manual how to register a dll into windows."
        ZenV = 2010
NoError:
    End If
    Initialize_Controller
    ScanStop = False
    ViewerGuiServer.FcsSelectLsmImagePositions = True
    
    If ZenV > 2010 Then
        fileFormatczi.value = True
    Else
        fileFormatlsm.value = True
    End If
    
    'fill ComboBox for FCS points per object
    For i = 1 To 6
        fcsPointsComboBox.AddItem CStr(i)
    Next i
    fcsClassComboBox.AddItem "nuc"
    fcsClassComboBox.AddItem "cyt"
    fcsClassComboBox.AddItem ""
    fcsPointsComboBox.ListIndex = 0
    fcsClassComboBox.ListIndex = 0
    fcsClassListBox.ColumnCount = 2
    fcsClassListBox.ColumnWidths = "30;80"
    
    'initialize Grid. This will be a row=1, Col, subRow=1, subCol = 1 Grid
    Set Grid = New AGrid
    Grid.initializeToZero
    
    StageSettings MirrorX, MirrorY, ExchangeXY
    
    'remove all FCS positions (this is for ZENv < 2011)
    FcsPositions.PositionListSize = 0
    updateListFcsPoints
    strIconPath = Application.ProjectFilePath & "\resources\fcsrunner_icon.ico"
    ' Get the icon from the source
    lngIcon = ExtractIcon(0, strIconPath, 0)
    ' Get the window handle of the userform
    lnghWnd = FindWindow("ThunderDFrame", Me.Caption)
    'Set the big (32x32) and small (16x16) icons
    SendMessage lnghWnd, WM_SETICON, True, lngIcon
    SendMessage lnghWnd, WM_SETICON, False, lngIcon
    Me.Show
End Sub

''''Imaging and FCS pipeline ''''''''''''''''''''''''''''''''''''''''''''''''''''''

''
'   PreImgFCSButton_Click()
'   Make new Image, and perform FCS, save FCSpoints
''
Private Sub PreImgFCSButton_Click()
    FcsRecord True, False
End Sub

''
'   FcsPostImageButton_Click()
'   perform FCS, acquire image, save FCSpoints
''
Private Sub FcsPostImageButton_Click()
    FcsRecord False, True
End Sub

''
'   PreImgFCSPostButton_Click()
'   Make new Image, save FCSpoints, perform FCS, make PostFcsImg
''
Private Sub PreImgFCSPostImgButton_Click()
    FcsRecord True, True
End Sub



''''
'   FcsRecord(PreImg As Boolean, PostImg As Boolean)
'       [PreImg]    In - If True acquire an image prior FCS
'       [PosImg]    In - If True acquire and image ater FCS
'   Default save actual recording, save FCSpositions and perform FCS measurement
''''
Private Sub FcsRecord(PreImg As Boolean, PostImg As Boolean)
    
    Dim Success As Boolean 'dummy variable
    Dim locRecording As DsRecording
    Dim fcsPosPx() As Vector
    Dim fcspos() As Vector
    Dim cntImgPx As Vector
    Dim iExp As Integer
    Dim iCol As Long
    Dim idx As Long
    Dim fileID As String
    Dim fcsPath As String
    Dim imgPath As String
    Dim currentPosition As Vector
    Dim fcspts_cls() As Integer
    Dim fcsname_cls() As String
    StopAcquisition
    If Grid.isGridEmpty Then
        MsgBox "Nothing to do!", VbExclamation, "FCSRunner"
        Exit Sub
    End If
    'consistency checks
    If OutputFolderTextBox.value = "" Then
        MsgBox "No Outputolder!", VbExclamation, "FCSRunner"
        Exit Sub
    End If
    SetDatabase
    SetFileName
    If GetFcsPositionListLength = 0 Then
        MsgBox "No points selected for FCS!", VbExclamation, "FCSRunner"
        Exit Sub
    End If
    
    getClassFcsPoints fcspts_cls, fcsname_cls
    If fcspts_cls(UBound(fcspts_cls)) = 0 Then
        MsgBox "Select number and name of FCS points", VbExclamation, "FCSRunner"
        Exit Sub
    End If
    
    'check if number of fcsPts correspond to number of fcspoints per object
    For iCol = 1 To Grid.numCol
        fcspos = Grid.getFcsPositions(1, iCol, 1, 1)
        If isPosArrayEmpty(fcspos) Then
            ListPos.ListIndex = iCol - 1
            MsgBox "No Fcs points for current stage position have been defined!"
            Exit Sub
        End If
        If Not ((UBound(fcspos) + 1) Mod fcspts_cls(UBound(fcspts_cls))) = 0 Then
            ListPos.ListIndex = iCol - 1
            MsgBox "Total number of fcs points (= " & UBound(fcspos) + 1 & ") must be a multiplier" & _
            " of sum of points for each object (=" & fcspts_cls(UBound(fcspts_cls)) & ").", VbExclamation, "FCSRunner"
            Exit Sub
        End If
    Next iCol
    

        
    For iCol = 1 To Grid.numCol
        ListPos.ListIndex = iCol - 1
        ' this moves to the first Z-position
        MoveStageListPos iCol
        If checkBoxDF.value Then
            ' First position is used to initialize Definite Focus
            If iCol = 1 Then
                If initializeDF Then
                   executeDF
                End If
             Else
                executeDF
             End If
        End If
        currentPosition = getCurrentPosition
        Grid.setThisZ currentPosition.Z
        updatePosList
        fcspos = Grid.getFcsPositions(1, iCol, 1, 1)
        For idx = 0 To UBound(fcspos)
            fcspos(idx).Z = currentPosition.Z * 0.000001
        Next idx
        Grid.setFcsPositions fcspos, 1, iCol, 1, 1
        setFcsPositionVec Grid.getFcsPositions(1, iCol, 1, 1)
        updateListFcsPoints
        SleepWithEvents (1000)
        IsAcquiring = True
        iExp = 1
        
        While FileExist(OutputFolderTextBox.value & format(iExp, "0000") & UnderScore & BaseNameTextBox.value & ".fcs")
            iExp = iExp + 1
        Wend
        
        'force central slice of imaging Z position to  be same position as FCS
        'If Not Recenter_pre(FcsPositions.PositionZ(0) * 1000000, Success, ZenV) Then
        '    GoTo Abort
        'End If
    
        'Acquire preFCS image
        fileID = format(iExp, "0000") & UnderScore & BaseNameTextBox.value
        Set locRecording = Lsm5.DsRecording
        cntImgPx = imageSizePx(locRecording)
        'center of image in px
        cntImgPx.X = (cntImgPx.X - 1) / 2
        cntImgPx.Y = (cntImgPx.Y - 1) / 2
        cntImgPx.Z = (cntImgPx.Z - 1) / 2
        If PreImg Then
            NewRecordGui GlobalRecordingDoc, "Img" & " " & fileID, ZEN
            setMetaPositionImg currentPosition
            
            If Not ScanToImage(GlobalRecordingDoc) Then
                GoTo Abort
            End If
            imgPath = OutputFolderTextBox.value & fileID
            saveImgxml imgPath & imgFileExtension & ".xml", cntImgPx, imgPath & imgFileExtension, currentPosition
            SaveDsRecordingDoc GlobalRecordingDoc, imgPath & imgFileExtension, imgFileFormat
        End If
    
        'Fcs Measurement
        fileID = format(iExp, "0000") & UnderScore & BaseNameTextBox.value
        NewFcsRecordGui GlobalFcsRecordingDoc, GlobalFcsData, "Fcs" & " " & fileID, ZEN
        If Not CleanFcsData(GlobalFcsRecordingDoc, GlobalFcsData) Then
            GoTo Abort
        End If
        If Not ScanToFcs(GlobalFcsRecordingDoc, GlobalFcsData) Then
            GoTo Abort
        End If
        fcsPath = OutputFolderTextBox.value & fileID
        SaveFcsMeasurement GlobalFcsData, GlobalFcsRecordingDoc, fcsPath & ".fcs"
        
        'Scan and save record after FCS
        If PostImg Then
            fileID = format(iExp, "0000") & UnderScore & BaseNameTextBox.value & FNSep & "postFCS"
            NewRecordGui GlobalRecordingDoc, "Img" & " " & fileID, ZEN
            setMetaPositionImg currentPosition
            If Not ScanToImage(GlobalRecordingDoc) Then
                GoTo Abort
            End If
            saveImgxml OutputFolderTextBox.value & fileID & imgFileExtension & ".xml", _
            cntImgPx, OutputFolderTextBox.value & fileID & imgFileExtension, currentPosition
            SaveDsRecordingDoc GlobalRecordingDoc, OutputFolderTextBox.value & fileID & imgFileExtension, imgFileFormat
            If Not PreImg Then
                imgPath = OutputFolderTextBox.value & fileID
            End If
        End If
        fcsPosPx = fcsPos2imgPx(locRecording, currentPosition, getFcsPositionVec)
        SaveFcsPositionList fcsPath, fcsPosPx, imgPath & imgFileExtension, currentPosition, fcsname_cls, fcspts_cls
        
        SleepWithEvents (1000)
    Next iCol
Abort:
    StopButtonState False
    ScanStop = False
    IsAcquiring = False
End Sub


Private Sub StopButton_Click()
    ScanStop = True
    If IsAcquiring Then
        StopButtonState ScanStop
    Else
        StopButtonState False
    End If
End Sub

Private Sub StopButtonState(State As Boolean)
    If State Then
        StopButton.BackColor = &HFF
    Else
        StopButton.BackColor = &H8000000A
    End If
End Sub

'''''''Manage Fcs Points. For each stage/grid position we can define FCS points''''''''''

''
' add fcs point to list box and grid from position of selection cross
''
Private Sub AddFcsPointButton_Click()
    Dim posFCS As Vector
    Dim posFCSa() As Vector
    Dim posStage As Vector
    Dim fcspts_cls() As Integer
    Dim fcsname_cls() As String
    Dim locRecording As DsRecording
    
    
    If fcsClassListBox.ListCount = 0 Then
        MsgBox "First define Fcs points per object", VbInformation, "FcsRunner"
        Exit Sub
    End If
    If GlobalRecordingDoc Is Nothing Then
        Set locRecording = Lsm5.CreateBackupRecording
        locRecording.Copy Lsm5.DsRecording
    Else
        Set locRecording = Lsm5.CreateBackupRecording
        locRecording.Copy GlobalRecordingDoc.Recording
    End If
    'get bane and number of Fcs points per class/location
    getClassFcsPoints fcspts_cls, fcsname_cls
    posStage = getCurrentPosition
    
    'create a new grid position if none is present or maximal number of fcs points per object
    If ListPos.ListCount < 1 Or (ListFcsPoints.ListCount = fcspts_cls(UBound(fcspts_cls)) And oneImagePerObjectClick) Then
        Grid.updateGridSizePreserve 1, Grid.numCol + 1, 1, 1
        Grid.setPt posStage, True, 1, Grid.numCol, 1, 1
        updatePosList
        ListPos.ListIndex = Grid.numCol - 1
    End If
    
    'get FCS position
    posFCS = getFcsPosition
    'Force FCS position at position of the image
    posFCS.Z = Grid.getZ(1, ListPos.ListIndex + 1, 1, 1) * 0.000001
    'round position so that it is exactly on a pixel
    ReDim posFCSa(0 To 0)
    posFCSa(0) = posFCS
    posStage.Z = Grid.getZ(1, ListPos.ListIndex + 1, 1, 1)
    posFCSa = fcsPos2imgPx(locRecording, posStage, posFCSa)
    
    posFCSa(0).X = Round(posFCSa(0).X)
    posFCSa(0).Y = Round(posFCSa(0).Y)
    posFCSa = imgPx2fcsPos(locRecording, posStage, posFCSa)
    
    setFcsPosition posFCSa(0), GetFcsPositionListLength
    Grid.setFcsPositions getFcsPositionVec, 1, ListPos.ListIndex + 1, 1, 1
    updateListFcsPoints
    ListFcsPoints.ListIndex = ListFcsPoints.ListCount - 1
    totalFCSpoints
End Sub

''
' remove selected point
''
Private Sub RemoveFcsPointButton_Click()
    Dim idx As Long
    Dim idxL As Long
    idxL = ListFcsPoints.ListIndex
    If ListFcsPoints.ListCount = 0 Or ListFcsPoints.ListIndex < 0 Or ListPos.ListIndex < 0 Then
        Exit Sub
    End If
    If ListFcsPoints.ListIndex < FcsPositions.PositionListSize - 1 Then
        For idx = ListFcsPoints.ListIndex + 1 To FcsPositions.PositionListSize - 1
            FcsPositions.PositionX(idx - 1) = FcsPositions.PositionX(idx)
            FcsPositions.PositionY(idx - 1) = FcsPositions.PositionY(idx)
            FcsPositions.PositionZ(idx - 1) = FcsPositions.PositionZ(idx)
        Next idx
    End If
    FcsPositions.PositionListSize = FcsPositions.PositionListSize - 1
    Grid.setFcsPositions getFcsPositionVec, 1, ListPos.ListIndex + 1, 1, 1
    updateListFcsPoints
    If ListFcsPoints.ListCount > 0 Then
        If idxL > 0 Then
            ListFcsPoints.ListIndex = idxL - 1
        Else
            ListFcsPoints.ListIndex = 0
        End If
    End If
    ViewerGuiServer.UpdateFcsPositions
    totalFCSpoints
End Sub

''
' Remove all FCS points
''
Private Sub RemoveAllFcsPointsButton_Click()
    ClearFcsPositionList
    updateListFcsPoints
    If ListPos.ListIndex >= 0 Then
        Grid.setFcsPositions getFcsPositionVec, 1, ListPos.ListIndex + 1, 1, 1
    End If
    totalFCSpoints
End Sub


Private Sub updateListFcsPoints()
    Dim i As Long
    Dim pos As Vector
    ListFcsPoints.Clear
    For i = 0 To GetFcsPositionListLength - 1
        ListFcsPoints.AddItem
        pos = getFcsPosition(i)
        ListFcsPoints.List(i, 0) = i + 1
        ListFcsPoints.List(i, 1) = CStr(Round(pos.X * 10 ^ 6, PrecXY))
        ListFcsPoints.List(i, 2) = CStr(Round(pos.Y * 10 ^ 6, PrecXY))
        ListFcsPoints.List(i, 3) = CStr(Round(pos.Z * 10 ^ 6, PrecXY))
    Next i
    If ListFcsPoints.ListCount > 0 Then
        ListFcsPoints.ListIndex = 0
    End If
End Sub

'''' Manage Grid/stage positions ''''''''''''''''''''''''''''''''''''''''''''''''''

''
' add a stage position to the grid
''
Private Sub AddPosButton_Click()
    Dim pos As Vector
    If fcsClassListBox.ListCount = 0 Then
        MsgBox "First define Fcs points per object", VbInformation, "FcsRunner"
        Exit Sub
    End If
    pos = getCurrentPosition
    Grid.updateGridSizePreserve 1, Grid.numCol + 1, 1, 1
    Grid.setPt pos, True, 1, Grid.numCol, 1, 1
    updatePosList
    Grid.iCol = Grid.numCol
    ListPos.ListIndex = ListPos.ListCount - 1
    totalFCSpoints
End Sub

''
' update FCS positions to positions stored for Grid point but do not move stage
''
Private Sub ListPos_Click()
    If ListPos.ListIndex < 0 Then
        Exit Sub
    End If
    setFcsPositionVec Grid.getFcsPositions(1, ListPos.ListIndex + 1, 1, 1)
    updateListFcsPoints
End Sub

''
' update FCS positions to positions stored for Grid point and move stage to new positions
''
Private Sub ListPos_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    MoveStageListPos ListPos.ListIndex + 1
End Sub

''
' Move stage position to grid position : 1, idx, 1, 1
''
Private Sub MoveStageListPos(idx As Long)
    Dim cPos As Vector
    Dim nPos As Vector
    Dim valid As Boolean
    If idx < 1 Then
        Exit Sub
    End If
    cPos = getCurrentPosition
    Grid.getPt nPos, valid, 1, idx, 1, 1
    Grid.iCol = idx
    setFcsPositionVec Grid.getFcsPositions(1, idx, 1, 1)
    updateListFcsPoints
    If Round(cPos.X, PrecXY) <> Round(nPos.X, PrecXY) Or Round(cPos.Y, PrecXY) <> Round(nPos.Y, PrecXY) Then
        If ZSafeDown <> 0 Then
            If Not FailSafeMoveStageZ(cPos.Z - ZSafeDown) Then
                Exit Sub
            End If
        End If
        If Not FailSafeMoveStageXY(nPos.X, nPos.Y) Then
            Exit Sub
        End If
    End If
    If Round(cPos.Z, PrecZ) <> Round(nPos.Z, PrecZ) Then
        If Not FailSafeMoveStageZ(nPos.Z) Then
            Exit Sub
        End If
    End If
End Sub

'''
' update ListBox ListPos with positions from grid
'''
Private Sub updatePosList()
    Dim idx As Integer
    ListPos.Clear
    If Grid.isGridEmpty Then
        FcsPositions.PositionListSize = 0
        updateListFcsPoints
        ViewerGuiServer.UpdateFcsPositions
        Exit Sub
    End If
    Grid.setIndeces 1, 1, 1, 1
    Do
        ListPos.AddItem
        ListPos.List(ListPos.ListCount - 1, 0) = ListPos.ListCount
        ListPos.List(ListPos.ListCount - 1, 1) = Grid.getThisX
        ListPos.List(ListPos.ListCount - 1, 2) = Grid.getThisY
        ListPos.List(ListPos.ListCount - 1, 3) = Grid.getThisZ
    Loop While Grid.nextGridPt(True)
End Sub
    

''
' remove selected point
''
Private Sub RemovePosButton_Click()
    Dim idx As Long
    idx = ListPos.ListIndex
    If ListPos.ListCount = 0 Or idx < 0 Then
        Exit Sub
    End If
    Grid.delPt idx + 1
    updatePosList
    If ListPos.ListCount = 0 Then
        Exit Sub
    End If
    If idx = 0 Then
        ListPos.ListIndex = 0
    Else
        ListPos.ListIndex = idx - 1
    End If
    totalFCSpoints
End Sub

''
' remove all position
''
Private Sub RemoveAllPosButton_Click()
    Grid.initializeToZero
    updatePosList
    FcsPositions.PositionListSize = 0
    updateListFcsPoints
    totalFCSpoints
End Sub

''
' update ListPos listBox with positions stored in Grid
''
Private Sub UpdatePosButton_Click()
    Dim idx As Integer
    idx = ListPos.ListIndex
    If ListPos.ListIndex >= 0 Then
        Grid.setPt getCurrentPosition, True, 1, idx + 1, 1, 1
        updatePosList
        ListPos.ListIndex = idx
    End If
End Sub

''' Manage class and # fcs points per object '''''''''''''''''''''''''''''''''''''''''''''''

'''
' add number and location/class name for fcs points
'''
Private Sub AddClassButton_Click()
    fcsClassListBox.AddItem
    fcsClassListBox.List(fcsClassListBox.ListCount - 1, 0) = fcsPointsComboBox.value
    fcsClassListBox.List(fcsClassListBox.ListCount - 1, 1) = fcsClassComboBox.value
    fcsClassListBox.ListIndex = fcsClassListBox.ListCount - 1
End Sub

'''
' delete number and location/class name for fcs points
'''
Private Sub DelClassButton_Click()
    Dim idx As Integer
    idx = fcsClassListBox.ListIndex
    If idx < 0 Then
        MsgBox "Click on list entry to delete it!", VbExclamation, "FCSRunner"
        Exit Sub
    End If
    fcsClassListBox.RemoveItem fcsClassListBox.ListIndex
    If fcsClassListBox.ListCount > 0 Then
        If idx > 0 Then
            fcsClassListBox.ListIndex = idx - 1
        Else
            fcsClassListBox.ListIndex = 0
        End If
    End If
End Sub

''
' get numner of fcs points and class name. Number of fcs points is cumulative
''
Private Sub getClassFcsPoints(fcspts() As Integer, fcsName() As String)
    Dim idx As Long
    Dim totfcs As Integer
    totfcs = 0
    If fcsClassListBox.ListCount <= 0 Then
        ReDim fcsName(0)
        ReDim fcspts(0)
        fcspts(0) = 0
        fcsName(0) = ""
        Exit Sub
    End If
    ReDim fcsName(0 To fcsClassListBox.ListCount - 1)
    ReDim fcspts(0 To fcsClassListBox.ListCount - 1)
    fcspts(0) = CInt(fcsClassListBox.List(0, 0))
    fcsName(0) = CStr(fcsClassListBox.List(0, 1))
    For idx = 1 To fcsClassListBox.ListCount - 1
        fcspts(idx) = fcspts(idx - 1) + CInt(fcsClassListBox.List(idx, 0))
        fcsName(idx) = CStr(fcsClassListBox.List(idx, 1))
    Next idx
    
End Sub

''
' show total number of acquired FCS points
''
Private Sub totalFCSpoints()
    Dim idx As Long
    Dim tot As Integer
    Dim pos() As Vector
    If Grid.isGridEmpty Then
        totalFcsPtsBox.value = 0
        Exit Sub
    End If
    For idx = 1 To Grid.numCol
        pos = Grid.getFcsPositions(1, idx, 1, 1)
        If Not isPosArrayEmpty(pos) Then
            tot = tot + UBound(pos) + 1
        End If
    Next idx
    totalFcsPtsBox.value = tot
End Sub

'''' Manage file output '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

''
' Only update the outputfolder when enter is pressed. This avoids creating a folder at every keystroke
''
Private Sub OutputFolderTextBox_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If KeyCode = 13 Then 'this is the enter key
        SetDatabase
    End If
End Sub

Private Sub BaseNameTextBox_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If KeyCode = 13 Then 'this is the enter key
        SetFileName
    End If
End Sub

Private Sub SetFileName()
    If BaseNameTextBox.value <> "" Then
        UnderScore = FNSep
    Else
        UnderScore = ""
    End If
End Sub

''
' set folder where to save data
''
Private Sub SetDatabase()

    GlobalDataBaseName = OutputFolderTextBox.value

    If Not GlobalDataBaseName = "" Then
        If VBA.Right(GlobalDataBaseName, 1) <> "\" Then
            OutputFolderTextBox.value = OutputFolderTextBox.value + "\"
            GlobalDataBaseName = OutputFolderTextBox.value
        End If
        On Error GoTo ErrorHandleDataBase
        If Not CheckDir(GlobalDataBaseName) Then
            Exit Sub
        End If
    End If

    Exit Sub
ErrorHandleDataBase:
    MsgBox "Could not create output Directory " & GlobalDataBaseName
End Sub


Private Sub OutputFolderButton_Click()
''''''''''''''''''''''''''''''''''''''
' Set output folder
''''''''''''''''''''''''''''''''''''''
    Dim Filter As String, FileName As String
    Dim Flags As Long
    Dim DefDir As String

    Flags = OFN_PATHMUSTEXIST Or OFN_HIDEREADONLY Or OFN_NOCHANGEDIR Or OFN_EXPLORER Or OFN_NOVALIDATE

    'Filter = "All Data (*.*)" & Chr$(0) & "*.*"
    If GlobalDataBaseName = "" Then
        DefDir = "C:\"
    Else
        DefDir = GlobalDataBaseName
    End If
    
    FileName = CommonDialogAPI.ShowOpen(Filter, Flags, "*.*", DefDir, "Select output folder")
    If Len(FileName) > 3 Then
        FileName = VBA.Left(FileName, Len(FileName) - 3)
        OutputFolderTextBox.value = FileName
        SetDatabase
    End If
    
End Sub

Private Sub fileFormatlsm_Click()
    imgFileFormat = eAimExportFormatLsm5
    imgFileExtension = ".lsm"
End Sub

'''set global variables for file format
Private Sub fileFormatczi_Click()
#If ZENvC > 2010 Then
    imgFileFormat = eAimExportFormatCzi
    imgFileExtension = ".czi"
#Else
    MsgBox "Your ZEN version does not support czi files", VbInformation
    fileFormatlsm.value = True
#End If
End Sub
